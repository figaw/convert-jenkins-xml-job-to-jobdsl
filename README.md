# Convert Jenkins XML Job to JobDSL

If you wrap an entire `config.xml` in a configure block from JobDSL.
You can very easily "convert" an oldschool Jenkins Job to JobDSL.

Of course it's not supposed to keep being XML,
but it's a great start for an iterative conversion!

## Example

_Show, don't tell!_ The [example](example) folder contains
    a complete example of a `config.xml` converted to "JobDSL."

## Converting XML to JobDSL

Prerequisites:

- The `config.xml` for the job you want to convert,
    look at the "Get the config.xml for a job"-section.

### Creating a JobDSL .groovy file

1. Make a copy (X) of the `xml_to_jobdsl_template.groovy` template file.
    > NB: A "JobDSL .groovy file" must be named with letters and underscores.
1. Replace the `XML JOB HERE`-part with the contents of your `config.xml` file.
1. Copy the contents of the `config.xml` and paste it into X.
1. Remove `<?xml version='1.1' encoding='UTF-8'?>`.
1. Escape all `\` and `$` with backslashes.
1. Give the job a name other than `replace-me-jobdsl`. NB: don't use whitespaces.
    > Congratulations! You've now converted your job to JobDSL.

The following sections are about using the JobDSL to generate a "new" `config.xml`,
    you can compare to the "old" `config.xml`.

## Generating a config.xml from a JobDSL .groovy file

> I've compared _one_ (however, very complex) job, generated in jenkins
> and the commandline, and the resulting `xml`-files were identical.
> The result _can_ differ slightly, see [Run a DSL Script locally][link-dsl-locally].
> But in the end you'll still be comparing it to the "original" `config.xml`,
> and make an intelligent decision based on that.

### UI - generating config.xml

You need a Jekins server with the JobDSL plugin installed.

1. In Jenkins, create a new freestyle job. Give it a name.
1. Add a "Process Job DSLs" build step, select "Use the provided DSL script".
1. Paste the contents of X in the "DSL Script" box.
1. Save and run the job.
1. A new job should exist in Jenkins now.

### Commandline - generating config.xml

You need some `job-dsl-core-1.74-standalone.jar`-release of JobDSL.
    [It's available from the official documentation.][link-dsl-locally]

1. Use `java -jar job-dsl-core-1.74-standalone.jar folder/jobdsl.groovy`
    to generate a `job-name.xml` file.

## Verifying Equality

1. In a web browser, navigate to the `config.xml` of the original job.
1. Copy and paste the contents, with the encoding from the web browser,
    into a new file, e.g. `original-config.xml`.
1. In a web browser, navigate to the `config.xml` of your new generated job.
1. Copy and paste the contents, with the encoding from the web browser,
    into a new file, e.g. `generated-config.xml`.
1. Diff the two files and verify that the contents are conceptually the same.

Why copy it from the web browser?

- It will encode `&quot;` and `"` the same.
    JobDSL changes them from `&quot;` to `"` when it reads the XML.
- Jenkins will put empty tags in the XML, e.g. `<desc></desc>`.
    JobDSL will use the short-notation `</desc>` and so will the web browser.

> NB: I didn't test this in Internet Explorer 6.0.

## Get the config.xml for a job

### UI - config.xml

1. Navigate to job in Jenkins, go to "Configure".
1. Replace the `/configure` in the URL with `/config.xml`.
1. Download the `config.xml`-file or use `view source` and copy the contents.

### Commandline - config.xml

The `config.xml` for the job, will be located in `jenkins_home/jobs/<jobname>/config.xml`.

## What Now

- Use a seed-job for your jobs,
    and just enjoy that your XML is in JobDSL.

- Make changes to your job from the UI, and re-do all the steps,
    to "keep your XML in JobDSL."

- Use your JobDSL job as a baseline for conversion!
    Make a copy and gradually convert the XML-part to "actual" JobDSL.
    Verify equality by comparing your "new" `config` with the `config.xml` from the baseline.

## References

- [jenkinsci/job-dsl-plugin](https://github.com/jenkinsci/job-dsl-plugin)

[link-dsl-locally]: https://github.com/jenkinsci/job-dsl-plugin/wiki/User-Power-Moves#run-a-dsl-script-locally
